import numpy as np
from scipy.stats import norm, invgamma, t

class NiG():
    """Normal inverse-gamma prior"""
    
    def __init__(self, xi, nu, lam):
        """Constructor"""
        self.xi = xi                             # hyperparameter xi
        self.nu = float(nu)                      # hyperparameter nu
        self.p = self.xi.shape[0] - 1.           # number of parameters
        self.a_log = []                          # log of parameter a
        self.b_log = []                          # log of parameter b
        self.V_log = []                          # log of scaling matrix
        self.Ebeta_log = []                      # log of E[beta]
        self.Esigma2_log = []                    # log of E[sigma2]
        self.var_beta_log = []                   # log of var(beta)
        self.var_sigma2_log = []                 # log of var(sigma2)
        self.lam = lam
        
    def update(self, y, x):
        """Bayesian update by observation y and regressor X"""
        dt = np.hstack((y, x)).astype(float)
        prod = np.outer(dt, dt).astype(float)
        #print(dt.size)
        #print(prod.size)
        self.xi = self.lam*self.xi + prod
        self.nu = self.lam*self.nu + 1
        
    def log(self):
        """Logging"""
        self.a_log.append(self.a)
        self.b_log.append(self.b)
        self.V_log.append(self.V)
        self.Ebeta_log.append(self.Ebeta)
        self.var_beta_log.append(self.var_beta)
        self.Esigma2_log.append(self.Esigma2)
        self.var_sigma2_log.append(self.var_sigma2)
        
    @property 
    def Ebeta(self):
        """Estimate of theta"""
        return np.dot(self.V, self.xi[1:, 0])
        
    @property
    def var_beta(self):
        """Variance of beta estimate"""
        return np.diag(self.Esigma2 * self.V)

    @property
    def Esigma2(self):
        """Estimate of variance sigma^2"""
        return invgamma.mean(a=self.a, scale=self.b)

    @property
    def var_sigma2(self):
        """Variance of sigma2 estimate"""
        return invgamma.var(a=self.a, scale=self.b)
        
    @property
    def a(self):
        """Hyperparameter a"""
        return .5 * self.nu
        
    @property    
    def b(self):
        """Hyperparameter b"""
        return (self.xi[0,0] - self.xi[0,1:].dot(self.V).dot(self.xi[0,1:].T)) / 2.
        
    @property
    def V(self):
        """Hyperparameter V (scaling matrix)"""
        return np.linalg.inv(self.xi[1:,1:])
